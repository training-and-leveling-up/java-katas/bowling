package com.jgarfer.bowling;

public class Game {

    private int score;
    private int frame;
    private int roll;

    public Game() {
        score = 0;
        frame = 1;
        roll = 1;
    }

    public int score() {
        return score;
    }

    public void roll(int pins) {
        score += pins;



    }
}
