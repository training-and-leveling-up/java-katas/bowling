package com.jgarfer.bowling;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GameTest {

    @Test
    public void testInitialState() {
        Game game = new Game();
        assertEquals(0, game.score());
    }

    @Test
    public void test1() {
        Game game = new Game();
        game.roll(2);
        assertEquals(2, game.score());
    }

    @Test
    public void test2() {
        Game game = new Game();
        game.roll(2);
        game.roll(8);
        assertEquals(10, game.score());
    }

    @Test
    public void test3() {
        Game game = new Game();
        game.roll(2);
        game.roll(8);
        game.roll(2);
        assertEquals(14, game.score());
    }
}